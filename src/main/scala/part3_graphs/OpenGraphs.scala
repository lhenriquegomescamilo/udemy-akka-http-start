package part3_graphs

import akka.actor.ActorSystem
import akka.stream.scaladsl.{Broadcast, Concat, Flow, GraphDSL, Sink, Source}
import akka.stream.{ActorMaterializer, FlowShape, SinkShape, SourceShape}

object OpenGraphs extends App {

  implicit val system = ActorSystem("OpenGraphs")
  implicit val materialize = ActorMaterializer()

  /*
    A composeite source that concatantes 2 sources
     - Emits ALL the elements from the first source
     - Then ALL the elements from the second
   */

  val firstSource = Source(1 to 10)
  val secondSource = Source(42 to 10000)

  //  val sourceGraph = Source.fromGraph(
  //    GraphDSL.create() { implicit builder =>
  //      import akka.stream.scaladsl.GraphDSL.Implicits._
  //
  //      val concat = builder.add(Concat[Int](inputPorts = 2))
  //
  //      firstSource ~> concat
  //      secondSource ~> concat
  //
  //      SourceShape(concat.out)
  //    }
  //  )

  //  sourceGraph.to(Sink.foreach(println)).run()

  //  val sink1 = Sink.foreach[Int](x => println(s"Meaningful thing 1: $x"))
  //  val sink2 = Sink.foreach[Int](x => println(s"Meaningful thing 2: $x"))
  //  val sinkGraph = Sink.fromGraph(
  //    GraphDSL.create(){implicit builder =>
  //      import GraphDSL.Implicits._
  //      val broadcast = builder.add(Broadcast[Int](2))
  //      broadcast ~> sink1
  //      broadcast ~> sink2
  //
  //      SinkShape(broadcast.in)
  //    }
  //  )
  //  sourceGraph.to(sinkGraph).run()


  /**
   * Challenge - complex flow?
   * Writer your own flow that's composed of two others flows
   *  - One that adds 1 to a number
   *  - One that does number * 10
   */

  val incrementer = Flow[Int].map(_ + 1)
  val multiplier = Flow[Int].map(_ * 10)


  val flowGraph = Flow.fromGraph(
    GraphDSL.create() { implicit builder =>
      import GraphDSL.Implicits._

      val incrementerShape = builder.add(incrementer)
      val multiplierShape = builder.add(multiplier)

      incrementerShape ~> multiplierShape

      FlowShape(incrementerShape.in, multiplierShape.out)
    }
  )

//  firstSource.via(flowGraph).to(Sink.foreach[Int](println)).run()

  def fromSinkAndFlow[A, B](sink: Sink[A, _], source: Source[B,_]): Flow[A, B, _] =
    Flow.fromGraph(
      GraphDSL.create(){implicit builder =>
        val sourceShape = builder.add(source)
        val sinkShape = builder.add(sink)

        FlowShape(sinkShape.in, sourceShape.out)
      }
    )

  val f = Flow.fromSinkAndSourceCoupled(Sink.foreach[String](println), Source(1 to 100))

}
