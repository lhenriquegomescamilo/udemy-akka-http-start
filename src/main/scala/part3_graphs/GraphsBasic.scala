package part3_graphs

import akka.NotUsed
import akka.actor.ActorSystem
import akka.stream.scaladsl.{Balance, Broadcast, Flow, GraphDSL, Merge, RunnableGraph, Sink, Source, Zip}
import akka.stream.{ActorMaterializer, ClosedShape}

object GraphsBasic extends App {
  implicit val system = ActorSystem("GraphsBasic")
  implicit val materializer = ActorMaterializer()

  val input = Source(1 to 1000)
  val incrementer = Flow[Int].map(x => x + 1)
  val multiplier = Flow[Int].map(x => x * 10)
  val output = Sink.foreach[(Int, Int)](println)


  // steip 1 - setting up the fundamentals for the graphs
//  val graph = RunnableGraph.fromGraph(
//    GraphDSL.create() {
//      implicit builder: GraphDSL.Builder[NotUsed] =>
//        import GraphDSL.Implicits._ // Brings some nice operators  into scope
//
//        // step 2 - add the necessary compnents of the graphs
//        val broadcast = builder.add(Broadcast[Int](2))
//        val zip = builder.add(Zip[Int, Int])
//
//        // step 3 - tying up the component
//        input ~> broadcast
//
//
//        broadcast.out(0) ~> incrementer ~> zip.in0
//        broadcast.out(1) ~> multiplier ~> zip.in1
//        zip.out ~> output
//
//        // step 4 - return a close shape
//        ClosedShape
//    } // runnable graph
//  )


  /**
   * Exercise 1: Feed a source into 2 sinks at the same time (hint: use a broadcast)
   */


//  val firstSink = Sink.foreach[Int](x => println(s"First sink $x"))
//  val seconSink = Sink.foreach[Int](x => println(s"Second sink $x"))

//  val sourceToTwoSinksGraph = RunnableGraph.fromGraph(
//    GraphDSL.create() { implicit builder =>
//      import GraphDSL.Implicits._
//
//      // step 2 - declaring components
//      val broadcast = builder.add(Broadcast[Int](2))
//      input ~> broadcast ~> broadcast ~> firstSink // implicit port numbering
//      broadcast ~> seconSink
//
//      ClosedShape
//    }
//  )
  //  sourceToTwoSinksGraph.run()


  /**
   * Exercise 2: balance
   */

  import scala.concurrent.duration._

  val fastSource = input.throttle(5, 1 second)
  val slowSource = input.throttle(2, 1 second)

  val sink1 = Sink.fold[Int, Int](0)((count, _) => {
    println(s"SInk1 number of element $count")
    count + 1
  })
  val sink2 = Sink.fold[Int, Int](0)((count, _) => {
    println(s"SInk2 number of element $count")
    count + 1
  })


  val balanceGraphs = RunnableGraph.fromGraph(
    GraphDSL.create() { implicit builder =>
      import GraphDSL.Implicits._
      val merge = builder.add(Merge[Int](inputPorts = 2))
      val balance = builder.add(Balance[Int](outputPorts = 2))

      fastSource ~> merge ~> balance ~> sink1
      slowSource ~> merge
      balance ~> sink2
      ClosedShape
    }
  )

  balanceGraphs.run()


}
