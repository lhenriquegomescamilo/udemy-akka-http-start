package part1_recap

import akka.actor.ActorSystem
import akka.stream.{ActorMaterializer, OverflowStrategy}
import akka.stream.scaladsl.{Flow, Keep, Sink, Source}

import scala.util.{Failure, Success}

object AkkaStreamsRecap extends App {
  implicit val system: ActorSystem = ActorSystem("AkkaStreamsRecap")
  implicit val materializer: ActorMaterializer = ActorMaterializer()

  val source = Source(1 to 100)
  val sink = Sink.foreach[Int](println)
  val flow = Flow[Int].map(x => x + 1)

  import system.dispatcher

  val runnlabelGraph = source.via(flow).to(sink)
//  val simpleMaterializedValue = runnlabelGraph.run() // materialization

  // MATERIALIZED VALUE

  val sumSink = Sink.fold[Int, Int](0)((currentSum, element) => currentSum + element)
  //  val sumFuture = source.runWith(sumSink)
  //  sumFuture.onComplete {
  //    case Success(sum) => println(s"The sum of all the numbers from the simples source is: $sum")
  //    case Failure(exception) => println(s"Sum all the numbers from the simples source FAILED: $exception")
  //  }

  val anotherMaterializedValue = source.viaMat(flow)(Keep.right).toMat(sink)(Keep.left)
  //    .run()
  /*
    1 - materializing a graph means materializing ALL the component
    2 - a materialized value can be ANYTHING AT ALL
   */

  /*
    Backpressure actions
   - buffer elements
   - apply a strategy in case the buffer overflowa
   - fail the entire stream
   */

  val bufferedFlow = Flow[Int].buffer(10, OverflowStrategy.dropHead)

  source.async
    .via(bufferedFlow).async
    .runForeach({ e =>
      Thread.sleep(100)
      println(e)
    })
}
