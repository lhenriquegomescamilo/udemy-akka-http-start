package part1_recap

import akka.actor.SupervisorStrategy.{Restart, Stop}
import akka.actor.{Actor, ActorLogging, ActorSystem, OneForOneStrategy, PoisonPill, Props, Stash, SupervisorStrategy}
import akka.util.Timeout

object AkkaRecap extends App {

  class SimpleActor extends Actor with ActorLogging with Stash {
    override def receive: Receive = {
      case "stashThis" => stash()
      case "change handler NOW" =>
        unstashAll()
        context.become(anotherHandler)
      case "createChild" =>
        val childActor = context.actorOf(Props[SimpleActor], "myChild")
        childActor ! "hello"

      case "change" => context.become(anotherHandler)
      case message => println(s"I recieved: $message ")
    }

    def anotherHandler: Receive = {
      case message => println(s"I another recieve handler: $message ")
    }

    override def preStart(): Unit = {
      log.info("I am starting ")
    }

    override def supervisorStrategy: SupervisorStrategy = OneForOneStrategy() {
      case _: RuntimeException => Restart
      case _ => Stop
    }
  }

  val system = ActorSystem("AkkaRecap")

  val actor = system.actorOf(Props[SimpleActor], "SimpleActor")

  actor ! "Hello"

  actor ! PoisonPill

  import system.dispatcher

  import scala.concurrent.duration._

  system.scheduler.scheduleOnce(2 seconds) {
    actor ! "delayed happy birthday"
  }


  import akka.pattern.ask

  implicit val timeout = Timeout(3 seconds)
  val future = actor ? "question"

  import akka.pattern.pipe
  val anotherActor = system.actorOf(Props[SimpleActor], "anotherSimpleActor")
  future.mapTo[String].pipeTo(anotherActor)

}
