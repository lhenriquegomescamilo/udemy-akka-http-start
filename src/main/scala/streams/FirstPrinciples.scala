package streams

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.{Flow, Sink, Source}

import scala.concurrent.Future

object FirstPrinciples extends App {

  // Implicity key word
  implicit val system: ActorSystem = ActorSystem("FirstPrinciples")
  implicit val materializer: ActorMaterializer = ActorMaterializer()


  // Sources of data
  val source = Source(1 to 10)


  // Subcriber
  val sink = Sink.foreach[Int](println)


  // This expression called graph
  val graph = source.to(sink)

  // Every grapth need run in context materialize, In this momento a just change val called materializer to implicity in this context
  //  graph.run()


  // Introduction flows elements

  val flow = Flow[Int].map(x => x + 1)

  var sourcesWithFlow = source.via(flow)

  val flowWithSink = flow.to(sink)

  //  sourcesWithFlow.to(sink).run()

  //  sourcesWithFlow.to(flowWithSink).run()

  source.via(flow).to(sink).run()


  // nulls are not allowed
//  val illegalSource = Source.single[String](null)
//  illegalSource.to(Sink.foreach(println)).run()
  // use Options instead


  // many kinds of sources
  val finitSource = Source.single[Int](1)
  val anotherFinitSource = Source(List(1, 2, 3))
  val emptySource = Source.empty[Int]
  val inifiniteSource = Source(Stream.from(1))

  import scala.concurrent.ExecutionContext.Implicits.global

  val futureSource = Source.fromFuture(Future(42))

  // sinks
  val theMostBoringSink = Sink.ignore
  val forEachSink = Sink.foreach[String](println)
  val headSink = Sink.head[Int] // retrieves head and then closes the streams
  val foldSink = Sink.fold[Int, Int](0)((a, b) => a + b)

  // flows - usually mapped to collection operators
  val mapFlow = Flow[Int].map(x => x * 2)
  val takeFlow = Flow[Int].take(5)
  // drop, filter
  // Not have flatMap

  // source -> flow -> flow -> ... -> Sink

  val doubleFlowGraph = source.via(mapFlow).via(takeFlow).to(sink)
  doubleFlowGraph.run()

  // syntatic sugars

  val mapSource = Source(1 to 10).map(x => x * 2) // Same that Source(1 to 10).via(Flow[Int].map(x => x * 2))

  // run streams directly
  mapSource.runForeach(println) // same that mapSource.to(Sink.foreach[Int](println)).run()

  // OPERATORS = components

  /**
   * Exercise: create a stream that takes the names of person, then you will keep the first 2 names with length > 5 characters
   */

  val names = List("Alice", "Bob", "Charlie", "Davida", "Martin", "AkkaSTreams")

  val nameSource = Source(names)
  val longNameFlow = Flow[String].filter(x => x.length() > 5)
  val limitFlow = Flow[String].take(2)
  val nameSink = Sink.foreach[String](println)

  nameSource.via(longNameFlow).via(limitFlow).to(nameSink).run()

  nameSource.filter(_.length() > 5).take(2).runForeach(println)
}
