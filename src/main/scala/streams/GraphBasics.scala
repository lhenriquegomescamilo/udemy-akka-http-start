package streams

import akka.NotUsed
import akka.actor.ActorSystem
import akka.stream.scaladsl.{Balance, Broadcast, Flow, GraphDSL, Merge, RunnableGraph, Sink, Source, Zip}
import akka.stream.{ActorMaterializer, ClosedShape}

object GraphBasics extends App {

  implicit val system: ActorSystem = ActorSystem("GraphBasics")
  implicit val materializer: ActorMaterializer = ActorMaterializer()

  val input = Source(1 to 1000)
  val incrementer = Flow[Int].map(x => x + 1)
  val mutiplier = Flow[Int].map(x => x * 10)
  val output = Sink.foreach[(Int, Int)](println)


  // step 1 - Setting up the fundamentals for the graph
  val graph = RunnableGraph.fromGraph(
    GraphDSL.create() { implicit builder: GraphDSL.Builder[NotUsed] =>
      import GraphDSL.Implicits._ // bings some nice operator into scope

      // step 2 - add the necessary component of this graph
      val broadcast = builder.add(Broadcast[Int](2)) // fan-out operator

      val zip = builder.add(Zip[Int, Int]) // fan-in operator


      // steop 3 - Tying up the components
      input ~> broadcast

      broadcast.out(0) ~> incrementer ~> zip.in0

      broadcast.out(1) ~> mutiplier ~> zip.in1

      zip.out ~> output

      ClosedShape
    }
  )

  //  graph.run() // run the graph and mateirlize it
  /**
   * exercise 1: feed a source into 2 sinks at the  same time ( hint:  use a broadcast)
   */

  val firstSink = Sink.foreach[Int](x => println(s"First sink $x"))
  val secondSink = Sink.foreach[Int](x => println(s"Seconde sink $x"))


  // step 1
  val sourceToTwoSinkGraph = RunnableGraph.fromGraph(
    GraphDSL.create() { implicit builder =>
      import GraphDSL.Implicits._
      // step 2 - declaring the components
      val broadcast = builder.add(Broadcast[Int](2))

      // step 3 -> tyin up de components
      input ~> broadcast ~> firstSink // import port numbering
      broadcast ~> secondSink



      //      broadcast.out(0) ~> firstSink
      //      broadcast.out(1) ~> secondSink


      // step 4
      ClosedShape
    }
  )

  /**
   * Exercise 2: balance
   */

  import scala.concurrent.duration._

  val fastSource = input.throttle(5, 1 second)
  val slowSource = input.throttle(2, 1 second)

  val sink1 = Sink.fold[Int, Int](0)((count, _) => {
    println(s"Sink 1 number of elemetns: $count")
    count + 1
  })

  val sink2 = Sink.fold[Int, Int](0)((count, _) => {
    println(s"Sink 2 number of elemetns: $count")
    count + 1
  })

  val balanceGraph = RunnableGraph.fromGraph(
    GraphDSL.create() { implicit builder =>
      import GraphDSL.Implicits._

      val merge = builder.add(Merge[Int](2))
      val balance = builder.add(Balance[Int](2))

      fastSource ~> merge ~> balance ~> sink1
      slowSource ~> merge
      balance ~> sink2

      ClosedShape
    }
  )

  balanceGraph.run()
}
