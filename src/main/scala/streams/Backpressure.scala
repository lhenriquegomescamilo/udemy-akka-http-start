package streams

import akka.actor.ActorSystem
import akka.stream.scaladsl.{Flow, Sink, Source}
import akka.stream.{ActorMaterializer, OverflowStrategy}

object Backpressure extends App {
  implicit val actorSystem: ActorSystem = ActorSystem("Backpressure")
  implicit val actorMaterializer: ActorMaterializer = ActorMaterializer()

  val fatSource = Source(1 to 1000)
  val slowSink = Sink.foreach[Int] { x =>
    Thread.sleep(1000)
    println(s"Sink $x")
  }


  // Running the same actor
  //  fatSource.to(slowSink).run()


  // Running anothre Actor
  //  fatSource.async.to(slowSink).run()

  val simpleFlow = Flow[Int].map { x =>
    println(s"Incomming message $x")
    x + 1
  }


//  fatSource.async.via(simpleFlow).async.to(slowSink).run()

  /**
   * reactions to backpressure ( in order )
   *  - Try to slow down  if possible
   *  - buffer elements unti there more demand
   *  - drop down elements  from the buffer if it overflow
   *  - tear down/kill the whole stream (Failure)
   */

  val bufferedFlow = simpleFlow.buffer(5, overflowStrategy = OverflowStrategy.dropHead)
  fatSource.async
    .via(bufferedFlow).async
    .to(slowSink).async
    .run()
}
