package streams

import akka.NotUsed
import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.{Flow, Keep, RunnableGraph, Sink, Source}

import scala.concurrent.Future
import scala.util.{Failure, Success}

object MaterializedStreams extends App {
  implicit val system: ActorSystem = ActorSystem("MaterializedStreams")
  implicit val materialized: ActorMaterializer = ActorMaterializer()

  val simpleGraph: RunnableGraph[NotUsed] = Source(1 to 10).to(Sink.foreach(println))
  //  val simplesMaterializedValue: NotUsed = simpleGraph.run()(materialized)

  val source: Source[Int, NotUsed] = Source(1 to 10)
  val sink: Sink[Int, Future[Int]] = Sink.reduce[Int]((a, b) => a + b)
  val sumFuture: Future[Int] = source.runWith(sink)(materialized)

  //  sumFuture.onComplete {
  //    case Success(value) => println(s"The sum of element is $value")
  //    case Failure(exception) => println(s"The sum of elements is wrong, could not be computed: $exception")
  //  }

  // choossing the materialized values


  val simpleSource = Source(1 to 10)
  val simpleFlow = Flow[Int].map(x => x + 1)
  val simplSink = Sink.foreach[Int](println)
  val graph = simpleSource.viaMat(simpleFlow)(Keep.right).toMat(simplSink)(Keep.right)
  //  graph.run()(materialized).onComplete({
  //    case Success(_) => println("Stream processing finished.")
  //    case Failure(ex) => println(s"Stream processing failured with: $ex")
  //  })

  ugglyReduceMaterialized

  private def ugglyReduceMaterialized = {
    import system.dispatcher

    val eventualInt: Future[Int] = Source(1 to 10)
      .toMat(Sink.reduce[Int]((accumulator, current) => accumulator + current))(Keep.right)
      .run()(materialized)

    eventualInt.onComplete {
      case Success(v) => println(s"Value is $v")
      case Failure(ex) => println(s"The error is $ex")
    }
    //  1, 2, 3, 4, 5, 6, 7, 8, 9, 10
    //  1 + 2 = 3
    //  3 + 3 = 6
    //  6 + 4 = 10
    //  10 + 5 = 15
    //  15 + 6 = 21
    //  21 + 7 = 28
    //  28 + 8 = 36
    //  36 + 9 = 45
    //  45 + 10 = 55
  }
  import system.dispatcher

  Sink.foreach[Int](println).runWith(Source.single(42))


  Source(1 to 10)
    .runWith(Sink.reduce[Int](_ + _))(materialized)
    .onComplete({
      case Success(value) => println(s"Value is $value")
      case Failure(exception) => println(s"Error $exception")
    })

  Source(1 to 10)
    .runReduce(_ + _)(materialized)
    .onComplete({
      case Success(value) => println(s"Value is $value")
      case Failure(exception) => println(s"Error $exception")
    })

  Flow[Int].map(x => x * 2).runWith(simpleSource, simplSink)

  /**
   * - Return the last element out of  source (use Sink.last)
   * - compute total word count out of a streams of a sentences
   *  - map, reduce, fold
   */

  val future1 = Source(1 to 10).toMat(Sink.last)(Keep.right).run()
  val future2 = Source(1 to 10).runWith(Sink.last)


  val sentenceSource = Source(List(
    "Akka is Awesome",
    "I love streams",
    "Materialized value are killing me",
  ))

  val function: (Int, String) => Int = (currentWords, newSentence) => currentWords + newSentence.split(" ").length
  val worCountSink = Sink.fold[Int, String](0)(function)
  val g1: Future[Int] = sentenceSource.toMat(worCountSink)(Keep.right).run()
  val g2 = sentenceSource.runWith(worCountSink)
  val g3 = sentenceSource.runFold(0)(function)
  val worldCountFlow = Flow[String].fold[Int](0)(function)

  val g4 = sentenceSource.via(worldCountFlow).toMat(Sink.head)(Keep.right).run()
//  sentenceSource.viaMat(worldCountFlow)(Keep.left).toMat(Sink.head)(Keep.right).run()
  val g5 = sentenceSource.viaMat(worldCountFlow)(Keep.left).toMat(Sink.head)(Keep.right).run()

}
