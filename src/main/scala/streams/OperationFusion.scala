package streams

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.{Sink, Source}


object OperationFusion extends App {
  implicit val actorSystem: ActorSystem = ActorSystem("OperationFusion")
  implicit val materializer: ActorMaterializer = ActorMaterializer()

  //  val simpleSource = Source(1 to 1000)
  //  val simplesFlow = Flow[Int].map(_ + 1)
  //  val simpleFlow2 = Flow[Int].map(_ * 10)
  //  val simpleSink = Sink.foreach[Int](println)
  //
  //  simpleSource.via(simplesFlow).via(simpleFlow2).to(simpleSink).run()

  Source(1 to 3)
    .map(x => {
      println(s"Flow A $x");
      x
    }).async
    .map(x => {
      println(s"Flow B $x");
      x
    }).async
    .map(x => {
      println(s"Flow C $x");
      x
    }).async
    .runWith(Sink.ignore)

}
