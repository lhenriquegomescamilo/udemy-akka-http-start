package part2_lowlevelserver

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model._
import akka.stream.ActorMaterializer

object ExerciseLowLevelAkkaServer extends App {
    implicit val system: ActorSystem = ActorSystem("ExerciseLowLevelAkkaServer")
    implicit val materialize: ActorMaterializer = ActorMaterializer()

    val syncExerciseHandler: HttpRequest => HttpResponse = {
        case HttpRequest(HttpMethods.GET, Uri.Path("/"), _, _, _) => HttpResponse(
            entity = HttpEntity(
                ContentTypes.`text/html(UTF-8)`,
                "Hello from the exercise front-dor"
            )
        )

        case HttpRequest(HttpMethods.GET, Uri.Path("/about"), _, _, _) =>
            HttpResponse(
                entity = HttpEntity(
                    "Hello from the exercise fron door!"
                )
            )

        case request: HttpRequest =>
            request.discardEntityBytes()
            HttpResponse(
                StatusCodes.NotFound,
                entity = HttpEntity(
                    ContentTypes.`text/html(UTF-8)`,
                    "OOPs, you are in no man is land, sorry!"
                )
            )
    }
    Http().bindAndHandleSync(syncExerciseHandler, interface = "localhost", port = 8001)
}
